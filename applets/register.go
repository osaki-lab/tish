package applets

import (
	_ "github.com/future-architect/tish/applets/export"
	_ "github.com/future-architect/tish/applets/printenv"
	_ "github.com/future-architect/tish/applets/unset"

	_ "github.com/future-architect/tish/applets/cd"
	_ "github.com/future-architect/tish/applets/ls"
	_ "github.com/future-architect/tish/applets/pushd"

	_ "github.com/future-architect/tish/applets/chmod"
	_ "github.com/future-architect/tish/applets/cp"
	_ "github.com/future-architect/tish/applets/mkdir"
	_ "github.com/future-architect/tish/applets/mv"
	_ "github.com/future-architect/tish/applets/rm"
	_ "github.com/future-architect/tish/applets/rmdir"

	_ "github.com/future-architect/tish/applets/cat"
	_ "github.com/future-architect/tish/applets/echo"
	_ "github.com/future-architect/tish/applets/wc"

	_ "github.com/future-architect/tish/applets/sleep"
	_ "github.com/future-architect/tish/applets/timecmd"
)
