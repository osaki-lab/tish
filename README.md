# Tiny Shell (tish)

Tiny Shell written in Go

## License

Apache 2

Some code is based on the following code:

https://github.com/shirou/toybox
